//-----------------------------------------------------------------------
// <copyright file="TizenGalaxyWatch_IoTSingleAxisController.cs" company="John Helfrich">
//      Copyright (c) John Helfrich. MIT License
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
//the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
//to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
//THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>
// <author>John Helfrich</author>
//-----------------------------------------------------------------------

namespace TizenGalaxyWatch_IoTSingleAxisController
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// TizenGalaxyWatch_IoTSingleAxisController Application
    /// </summary>
    public class Program : global::Xamarin.Forms.Platform.Tizen.FormsApplication
    {
        /// <summary>
        /// Main program startup <see cref="Main(string[])"/>.
        /// </summary>
        /// <param name="args">Main args</param>
        public static void Main(string[] args)
        {
            var app = new Program();
            global::Xamarin.Forms.Platform.Tizen.Forms.Init(app);
            Tizen.Wearable.CircularUI.Forms.Renderer.FormsCircularUI.Init();
            app.Run(args);
        }

        /// <summary>
        /// Application startup On-Create Event <see cref="OnCreate"/>.
        /// </summary>
        protected override void OnCreate()
        {
            base.OnCreate();
            this.LoadApplication(new App());
        }
    }
}
