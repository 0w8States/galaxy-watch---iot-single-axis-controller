﻿//-----------------------------------------------------------------------
// <copyright file="App.cs" company="John Helfrich">
//      Copyright (c) John Helfrich. MIT License
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
//documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
//the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
//to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
//the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
//THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// </copyright>
// <author>John Helfrich</author>
//-----------------------------------------------------------------------
namespace TizenGalaxyWatch_IoTSingleAxisController
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using MQTTnet;
    using MQTTnet.Client;
    using Tizen.Wearable.CircularUI.Forms;
    using Xamarin.Forms;

    /// <summary>
    /// Main Tizen Wearable Application
    /// </summary>
    public class App : Application
    {
        /// <summary>MQTT Server Broker Address</summary>
        private const string MQTTBROKER = "broker.hivemq.com";

        /// <summary>MQTT Server Broker Port</summary>
        private const int MQTTPORT = 1883;

        /// <summary>MQTT Client Subscriber Topic</summary>
        private const string MQTTSUBTOPIC = "tizeniotdemo/position";

        /// <summary>MQTT Client Publisher Topic</summary>
        private const string MQTTPUBTOPIC = "tizeniotdemo/command";

        /// <summary>Label for position readout via MQTT</summary>
        private static Label positionData;

        /// <summary>MQTTnet Client Library Factory Type</summary>
        private static MqttFactory factory;

        /// <summary>MQTTnet Client Library Client Type</summary>
        private static IMqttClient mqttClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            // Launch the UI
            this.LaunchUI();
        }

        /// <summary>
        /// Initializes a new async task for MQTT Client control <see cref="RunAsync"/>.
        /// </summary>
        /// <returns>Returns the Task</returns>
        public static async Task RunAsync()
        {
            try
            {
                // Create a new MQTT client.
                factory = new MqttFactory();
                mqttClient = factory.CreateMqttClient();

                // Set the Client options
                var clientOptions = new MqttClientOptions
                {
                    ChannelOptions = new MqttClientTcpOptions
                    {
                        Server = MQTTBROKER,
                        Port = MQTTPORT
                    }
                };

                // Event Handler to the ApplicationMessageRecevied event
                mqttClient.ApplicationMessageReceived += (s, e) =>
                {
                    // Important to update the UI on a different thread, otherwise UI will crash with frequent updates
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        positionData.Text = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
                    });
                };

                mqttClient.Connected += async (s, e) =>
                {
                    await mqttClient.SubscribeAsync(new TopicFilterBuilder().WithTopic(MQTTSUBTOPIC).Build());
                    positionData.Text = "Connected";
                };

                // Attempt a connection to the broker
                try
                {
                    await mqttClient.ConnectAsync(clientOptions);
                }
                catch
                {
                    positionData.Text = "Failed";
                }
            }
            catch
            {
                positionData.Text = "Failed";
            }
        }

        /// <summary>
        /// Loads the UI for the Galaxy Watch <see cref="LaunchUI"/>.
        /// </summary>
        protected void LaunchUI()
        {
            // Status labels, Position will be updated on different thread
            positionData = new Label { TextColor = Color.MintCream, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.Center, Text = "MQTT Connecting..." };
            var heroLabel = new Label { TextColor = Color.GhostWhite, FontSize = 10, TranslationX = 360, FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.Center, Text = "MOTOR POSITION DATA" };

            // Create grids
            var contentGrid = new Grid { };
            var statusGrid = new Grid { Margin = new Thickness(0, 0, 0, 120) };
            var controlGrid = new Grid { Margin = new Thickness(0, 215, 0, 0) };

            // Create layouts
            var statusStacker = new StackLayout { VerticalOptions = LayoutOptions.CenterAndExpand };
            var controlStacker = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.CenterAndExpand };

            // Create buttons
            var jogPosButton = new Button { BackgroundColor = Color.FromHex("#660000"), TextColor = Color.MintCream, Text = "Jog +", HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Start, HeightRequest = 60, WidthRequest = 160 };
            var jogNegButton = new Button { BackgroundColor = Color.FromHex("#660000"), TextColor = Color.MintCream, Text = "Jog -", HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Start, HeightRequest = 60, WidthRequest = 160 };

            // Add the labels to their stacked layouts
            statusStacker.Children.Add(heroLabel);
            statusStacker.Children.Add(positionData);

            // Add the buttons to their stacked layout
            controlStacker.Children.Add(jogNegButton);
            controlStacker.Children.Add(jogPosButton);

            // Assign the stacked layouts to their grids
            statusGrid.Children.Add(statusStacker);
            controlGrid.Children.Add(controlStacker);

            // Assign the status grid and control grid to the main grid component
            contentGrid.Children.Add(statusGrid);
            contentGrid.Children.Add(controlGrid);

            // Display the content on main page
            this.MainPage = new CirclePage { Content = contentGrid };

            // Setup Jog Pos Events
            jogPosButton.Pressed += (s, e) => this.JogPosPressed(s, e);
            jogPosButton.Released += (s, e) => this.JogPosReleased(s, e);

            // Setup Jog Neg Events
            jogNegButton.Pressed += (s, e) => this.JogNegPressed(s, e);
            jogNegButton.Released += (s, e) => this.JogNegReleased(s, e);

            // Run Label Animation
            Task.Run(async () =>
            {
                while (true)
                {
                    await heroLabel.TranslateTo(-360, 0, 15000);

                    if (heroLabel.TranslationX <= -360)
                    {
                        heroLabel.TranslationX = 360;
                    }
                }
            });
        }

        /// <summary>
        /// On Start Event of the Application
        /// </summary>
        protected override void OnStart()
        {
            // Handle when your app starts
            Task.Run(async () => { await RunAsync(); });
        }

        /// <summary>
        /// On Sleep Event of the Application
        /// </summary>
        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        /// <summary>
        /// On Resume Event of the Application
        /// </summary>
        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        /// <summary>
        /// Generates and publishes message over MQTT
        /// </summary>
        /// <param name="topic">The topic where the message is published</param>
        /// <param name="mqttmessage">The message payload</param>
        private void PublishMessage(string topic, string mqttmessage)
        {
            // Start async task to publish message
            Task.Run(async () =>
            {
                var message = new MqttApplicationMessageBuilder()
                    .WithTopic(topic)
                    .WithPayload(mqttmessage)
                    .WithExactlyOnceQoS()
                    .WithRetainFlag()
                    .Build();

                await mqttClient.PublishAsync(message);
            });
        }

        /// <summary>
        /// Method to handle a Jog+ Pressed button event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Args </param>
        private void JogPosPressed(object sender, EventArgs e)
        {
            // Publish MQTT message to start motor movement
            this.PublishMessage(MQTTPUBTOPIC, "JOG+");
        }

        /// <summary>
        /// Method to handle a Jog+ Release button event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Args </param>
        private void JogPosReleased(object sender, EventArgs e)
        {
            // Publish MQTT message to stop motor movement
            this.PublishMessage(MQTTPUBTOPIC, "STOP");
        }

        /// <summary>
        /// Method to handle a Jog- Pressed button event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Args </param>
        private void JogNegPressed(object sender, EventArgs e)
        {
            // Publish MQTT message to start motor movement
            this.PublishMessage(MQTTPUBTOPIC, "JOG-");
        }

        /// <summary>
        /// Method to handle a Jog- Released button event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Args </param>
        private void JogNegReleased(object sender, EventArgs e)
        {
            // Publish MQTT message to stop motor movement
            this.PublishMessage(MQTTPUBTOPIC, "STOP");
        }
    }
}